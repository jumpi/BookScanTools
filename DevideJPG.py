# -*- coding: utf-8 -*-
"""
package DevideJPG.py

 Description : crop jpg with ImageMagick-7.0.8-Q16

 ImageMagick must be installed: sz http://www.imagemagick.org/script/download.php
 Output:
   return Code  : 0 if ok
                  1 otherwise """
from sys import stdout
                  
__version__    = "1.0"
__date__       = "2018/03/20"
__author__     = "Alten SO"

#-------------------- Import part -------------------------------------
# module importation 
#--------------------------------------------------------------------------

import sys,os.path,re,os
import easygui
import glob
import subprocess , shutil
import os, re
from os.path import basename
from unidecode import unidecode

def sorted_nicely( l ):
    """ Sorts the given iterable in the way that is expected.
 
    Required arguments:
    l -- The iterable to be sorted.
 
    """
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
    return sorted(l, key = alphanum_key)   
convertpath=subprocess.run(['where','convert'], stdout = subprocess.PIPE, stderr=subprocess.PIPE)
convert=[cmd.strip() for cmd in convertpath.stdout.decode('utf-8').split("\n") if "ImageMagick" in cmd]
if convertpath.returncode == 1:
   easygui.msgbox("ImageMagick must be installed and findable in the path (returncode == 1)")
   sys.exit(1)
convert="D:\PortableApps\ImageMagick-7.0.8-Q16\convert.exe"

if len(convert)< 1:
   easygui.msgbox("ImageMagick must be installed and findable in the path (%s)" % (len(convert)))
   sys.exit(1)
FileFormat=""
returncode=1
destFolder=None
prompt=""
try:    
    """convert Binder1.pdf Binder_%d.jpg"""
    #gather all link files
    linkargs = [ arg for arg in sys.argv[1:]  if '.' in arg  and os.path.basename(arg).split('.')[1] == 'lnk' ]
    #retrieve targets from links
    linkstringargument =[ win32com.client.Dispatch("WScript.Shell").CreateShortCut(arg).targetpath for arg in linkargs ]
    #gather other selection
    if len(sys.argv) > 2:
        currentfile=sys.argv[1]
        srcFolder=os.path.dirname(currentfile)
        ListFiles = [ os.path.basename(arg) for arg in sys.argv[1:]  if '.' not in arg or os.path.basename(arg).split('.')[1] != 'lnk' ]+linkstringargument        
        os.chdir(srcFolder)
        destFolder=srcFolder+"\\"+"DevidedJPG"
        if os.path.exists(destFolder):
              shutil.rmtree(destFolder, ignore_errors=True)
        os.makedirs(destFolder)
    elif len(sys.argv) == 2:
        currentfile=sys.argv[1]
        choice=easygui.ynbox('Proceed with all files in directory?', 'File selection', ('Yes', 'No'))
        if choice is True:
          srcFolder=os.path.dirname(currentfile)
          currentfileName,file_extension =os.path.splitext(os.path.basename(currentfile))
          os.chdir(srcFolder)
          ListFiles=glob.glob('*'+file_extension)
          destFolder=srcFolder+"\\"+"DevidedJPG"
          if os.path.exists(destFolder):
              shutil.rmtree(destFolder, ignore_errors=True)
          os.makedirs(destFolder)
          
    else:
        filepath=easygui.diropenbox("Chose Directory","Directory selection")
        if filepath==None:
            raise NameError('Canceled by user')
        os.chdir(filepath)
        ListFiles=glob.glob(filepath+'\\'+'*.jpg')
    ListFiles=sorted_nicely(ListFiles)
    FileListDevided=[]
    for currentfile in ListFiles:
        FileFormat=""
        
        srcFolder=os.path.dirname(currentfile)
        if destFolder is None:
            destFolder=srcFolder
        currentfileName,file_extension =os.path.splitext(os.path.basename(currentfile))
        
        targetFile=os.path.abspath(destFolder+'\\'+currentfileName+"_%d"+file_extension)
        targetFile1 = os.path.abspath(destFolder+'\\'+currentfileName+"_1"+file_extension)
        targetFile2 = os.path.abspath(destFolder+'\\'+currentfileName+"_2"+file_extension)
        FileListDevided.append(currentfileName+"_1"+file_extension)
        FileListDevided.append(currentfileName+"_2"+file_extension)
        #subprocess.STD
        print("currentfile:%s"%(currentfile))
            #extract chapters from mounted device     
        convertCommand=convert+' "'+currentfile+'"'+" -crop 2x1@ +repage -trim -border 10%x10% "+'"'+targetFile+'"'
        print(convertCommand)
        prompt=subprocess.run(convertCommand, stdout = subprocess.PIPE, stderr=subprocess.PIPE)
        returncode=prompt.returncode 
        if returncode == 1:
            raise NameError('Convert faile')
        #os.rename(destFolder+'\\'+currentfileName+"_0"+file_extension,targetFile2)
        #merge jpg in onepdf
    """convert *jpg allinone.pdf"""
    os.chdir(destFolder)
    #FileListDevided=sorted_nicely(FileListDevided)
    JPGFileList="DevidedJPGFileList.txt"
    f = open(JPGFileList, 'w')
    f.write("\n".join(FileListDevided))
    f.close()
    pdfname = "DevidedJPG.pdf"
    convertCommand = convert + ' @' + JPGFileList + ' ' + pdfname
    prompt = subprocess.run(convertCommand, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    returncode = prompt.returncode
        
   # easygui.msgbox(" Terminated ok :o\n Target:%s"%(targetFile))
    if returncode == 0:
        choice=easygui.buttonbox(" Terminated ok :o\n Target:%s"%(targetFile),"End of conversion Process", choices=('OK','LOG'),default_choice='OK')
        if choice == 'LOG':
            easygui.textbox(title="Log file of conversion", text=prompt.stdout.decode('utf-8').strip())
    else:
        choice=easygui.buttonbox(" Terminated ko :o(\n Target:%s"%(targetFile),"End of conversion Process", choices=('OK','LOG'),default_choice='OK')
        if choice == 'LOG':
            easygui.textbox(title="Log file of conversion", text=prompt.stdout.decode('utf-8').strip())    
except :
     easygui.exceptionbox()   
     if prompt:
         easygui.textbox(title="Log file of conversion", text=prompt.stdout.decode('utf-8').strip())
