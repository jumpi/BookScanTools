Attribute VB_Name = "NewMacros"
Sub ClSSMF()
Attribute ClSSMF.VB_ProcData.VB_Invoke_Func = "Normal.NewMacros.ClSSMF"
'
' ClSSMF Macro
'
'
    Selection.PasteSpecial DataType:=wdPasteText
End Sub
Sub FindmsoShapeMixed()
Dim oShp As Shape
With ActiveDocument
For Each oShp In .Shapes
With oShp
If .Type = 6 Then
    
        .Select
        Selection.GoTo What:=wdGoToBookmark, Name:="\page"
    Exit Sub
End If
End With
Next
End With
End Sub
Sub RemoveTextBox2()
    Dim shp As Shape
    Dim oRngAnchor As Range
    Dim sString As String

    For Each shp In ActiveDocument.Shapes
        If shp.Type = msoTextBox Then
            ' copy text to string, without last paragraph mark
            sString = Left(shp.TextFrame.TextRange.text, _
              shp.TextFrame.TextRange.Characters.Count - 1)
            If Len(sString) > 0 Then
                ' set the range to insert the text
                Set oRngAnchor = shp.Anchor.Paragraphs(1).Range
                ' insert the textbox text before the range object
                oRngAnchor.InsertBefore _
                  "Textbox start << " & sString & " >> Textbox end"
            End If
            shp.Delete
        End If
    Next shp
End Sub
Sub GetStyleName()

  MsgBox "Use " & Selection.Style.NameLocal & "Style for title?"

End Sub
Sub ReplaceAutoShape()
    Dim docNew As Document
    Dim shpStar As Shape
    Set docNew = ActiveDocument
    For Each shpStar In docNew.Shapes
        If shpStar.AutoShapeType = msoShapeMixed Then
            shpStar.AutoShapeType = msoShape32pointStar
        End If
    Next
End Sub
Sub Convert2Inline()
Dim i As Integer
For i = ActiveDocument.Shapes.Count To 1 Step -1
Select Case ActiveDocument.Shapes(i).Type
Case msoEmbeddedOLEObject, msoLinkedOLEObject, _
msoLinkedPicture, msoOLEControlObject, msoPicture
ActiveDocument.Shapes(i).ConvertToInlineShape
End Select
Next i
End Sub

Sub AddPeriod()

  Dim oPara As Word.Paragraph
  Dim rng As Range
  Dim text As String

  For Each oPara In ActiveDocument.Paragraphs
      If Len(oPara.Range.text) > 1 Then
         Set rng = ActiveDocument.Range(oPara.Range.Start, oPara.Range.End - 1)
         LastCaracter = Right(rng.text, 1)
         If LastCaracter Like "[A-Za-z]" Or LastCaracter = ChrW(187) Then
           rng.InsertAfter "."
         
         End If
         
      End If
   Next

End Sub
Function MajSansAccent$(ByVal Chaine$)
'Ti
OgrString = Chaine
Dim Pos As Integer
Const VAccent = "�����������������������������������������"
Const VSsAccent = "aaaaaaeeeeiiiioooooouuuuAAAAAACEEEEIIIIOO"
ValidChar = "[!a-zA-Z0-9 _\-]"
SetVAccent = "[�����������������������������������������]"
Dim Bcle&
For Bcle = 1 To Len(Chaine$)
  CurrentChar = Mid(Chaine$, Bcle, 1)
  If CurrentChar Like SetVAccent Then
     Pos = InStr(VAccent, CurrentChar)
     Chaine = Replace(Chaine, Mid(VAccent, Pos, 1), Mid(VSsAccent, Pos, 1))
  ElseIf CurrentChar Like ValidChar Then
     Chaine = Replace(Chaine, CurrentChar, "")
  End If
Next Bcle
MajSansAccent = Chaine
End Function




Sub ExportTitle1AsPDF()
'
' ExportTitle1 Macro
    Dim Title1Array() As Long
    Dim TitleTxtArray() As String
    Dim icount As Long
    Dim i As Long
    Dim pagestart As Long
    Dim pagesEnd As Long
    Dim TargetDirectory As String
    Dim TargetFile As String
    Dim MaxLenght As Integer
    MaxLenght = 45
    'Save author of document
    TrueAuthor = ActiveDocument.BuiltInDocumentProperties("Author")
    ExportAuthor = InputBox("Enter the author for the PDF files", "Input Author", TrueAuthor)
    If ExportAuthor = vbNullString Then
        Stop
    End If
    ActiveDocument.BuiltInDocumentProperties("Author") = ExportAuthor
    'go to top of document
    'Selection.HomeKey Unit:=wdStory

    PrefixPdfFileName = InputBox("Enter a prefix for pdf filenames without _", "Input Prefix", "")
    
    If PrefixPdfFileName <> "" Then
      PrefixPdfFileName = PrefixPdfFileName & "_"
    End If
    Folder = ActiveDocument.Path & "\\" & PrefixPdfFileName & ExportAuthor
    If Dir(Folder, vbDirectory) = "" Then
      MkDir Folder
    End If
    TargetDirectory = Folder
    With Application.FileDialog(msoFileDialogFolderPicker)
    .AllowMultiSelect = False
        If .Show = 0 Then
            MsgBox "Dialog cancelled"
            End
        End If
    TargetDirectory = .SelectedItems(1)
    End With

    
    'search for title 1
    Selection.Find.ClearFormatting
    Selection.Find.Style = ActiveDocument.Styles("Titre 1")
   
    With Selection.Find
        .text = ""
        .Replacement.text = ""
        .Forward = True
        .Wrap = wdFindContinue
        .Format = True
        .MatchCase = False
        .MatchWholeWord = False
        .MatchWildcards = False
        .MatchSoundsLike = False
        .MatchAllWordForms = False

        Do While .Execute
            icount = icount + 1
            ReDim Preserve Title1Array(1 To icount)
            Title1Array(icount) = Selection.Information(wdActiveEndPageNumber) 'current page
            ReDim Preserve TitleTxtArray(1 To icount)
            
            'change case of firt letter of each words in uppercase
            Selection.Range.Case = wdTitleWord
            text = Selection.text
            text = MajSansAccent$(text)
            If Len(text) > 1 Then
            'Title 1 text is empty
              TitleTxtArray(icount) = text
            End If
            
        Loop 'search next title1
    End With
    ' End of document
    ReDim Preserve Title1Array(1 To icount + 1)
    ' Set last value to number of pages + 1, to account for subtracting 1 in the loop
    Title1Array(icount + 1) = Selection.Information(wdNumberOfPagesInDocument) + 1
    Dim prompt As Integer
    For i = 1 To icount
        pagestart = Title1Array(i)
        pagesEnd = Title1Array(i + 1) - 1 'page end is (next title1 page) - 1
        If pagesEnd < pagestart Then
            pagesEnd = ActiveDocument.BuiltInDocumentProperties("number of Pages")
        End If
        CurrentTitle = TitleTxtArray(i)
        If Len(CurrentTitle) > MaxLenght Then
          CurrentTitle = Left(CurrentTitle, MaxLenght)
          'prompt = MsgBox("Title >" & CurrentTitle & "< cut to fit " & MaxLenght & " characters", vbOKCancel, "")
          
          'If prompt = vbCancel Then
          '  Stop
          'End If
          
        End If
        If Len(CurrentTitle) > 0 Then
            ActiveDocument.BuiltInDocumentProperties("Title") = CurrentTitle
            TargetFile = TargetDirectory & "\" & PrefixPdfFileName & Trim(CurrentTitle) & ".pdf"
            TargetFile = Trim(TargetFile)
            ActiveDocument.ExportAsFixedFormat OutputFileName:=TargetFile, _
                ExportFormat:=wdExportFormatPDF, _
                OpenAfterExport:=False, OptimizeFor:=wdExportOptimizeForPrint, _
                Range:=wdExportFromTo, From:=pagestart, To:=pagesEnd, _
                Item:=wdExportDocumentContent, IncludeDocProps:=True, KeepIRM:=False, _
                CreateBookmarks:=wdExportCreateHeadingBookmarks, BitmapMissingFonts:=False
        End If
    Next i
    ActiveDocument.BuiltInDocumentProperties("Author") = TrueAuthor
End Sub
